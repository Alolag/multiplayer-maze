sudo apt install build-essential autoconf automake libtool git libsdl2-dev binutils-dev uuid-dev libssl-dev cmake libasio-dev libboost-all-dev

mkdir -p 'build'
cd 'build'

#install libtcod https://github.com/libtcod/libtcod/tree/develop/buildsys/autotools
git clone -b '1.15.1' --single-branch https://github.com/libtcod/libtcod.git
cd libtcod/buildsys/autotools
autoreconf -i
./configure
make
sudo make install
