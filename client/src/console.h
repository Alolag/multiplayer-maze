#pragma once

#include <string>
#include <functional>

#include "state.h"

struct TMenuItem {
    std::string Name;
    std::function<bool()> Func; // If function return true, menu finished
    TMenuItem(const std::string& name, std::function<bool()> func);
};

struct TTableWithTwoColumnItem {
    std::string Left;
    std::string Right;
};

std::string getInput(const TState& state, const std::string text);
void ShowMenu(TState& state, const std::vector<TMenuItem>& items, const std::string& title, bool blockExit = false);
void ShowTableWithTwoColumn(TState& state, const std::vector<TTableWithTwoColumnItem>& items, const std::string& title, bool blockExit = false);
void PrintMap(TState& state, const std::vector<uint8_t>& map, int width, int height, int x, int y, bool printEmpty);
void PrintMessage(TState& state, const std::string& msg, TCOD_color_t c = {255, 255, 255});
void MultiplayAllColours(TState& state, float mul = 0.3f);
