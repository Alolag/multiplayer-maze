#include <libtcod/libtcod.h>
#include <string>
#include <stdio.h>

#include "state.h"
#include "client.h"
#include "console.h"

int main() {
    TCOD_console_set_custom_font("terminal.png", TCOD_FONT_TYPE_GREYSCALE | TCOD_FONT_LAYOUT_ASCII_INROW, 16, 16);
    if (TCOD_console_init_root(80, 50, "The Maze", false, TCOD_RENDERER_SDL) != 0) {
        return 1;
    }
    TState state(TCODConsole::root->get_data());
    std::vector<TMenuItem> test;
    auto test_func = []() {
        return true;
    };
    for (size_t i = 0; i < 70; ++i) {
        test.push_back(TMenuItem{"TestNum: " + std::to_string(i), test_func});
    }
    auto host = getInput(state, "Insert host:");
    auto port = std::stoul(getInput(state, "Insert port:"));
    TClient client(state);
    client.Connect(host, static_cast<unsigned short>(port));
    TCOD_quit();
    return 0;
}
