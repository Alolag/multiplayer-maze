#pragma once
#include <libtcod/libtcod.h>

class TState {
    bool isAutorisated = false;

public:
    TCOD_Console* Con; 

public:
    TState(TCOD_Console* con);

    void SetAutorisated();
    bool isValid() const;
};
