#include "console.h"

TMenuItem::TMenuItem(const std::string& name, std::function<bool()> func)
    : Name(name)
    , Func(func)
{}

std::string getInput(const TState& state, const std::string text) {
    std::string res;
    auto con = state.Con;
    while(state.isValid()) {
        int ch = TCOD_console_get_height(con) / 2;
        int cw = TCOD_console_get_width(con) / 2;
        TCOD_console_clear(con);
        TCOD_console_printf_ex(con, cw, ch - 1, TCOD_BKGND_DEFAULT, TCOD_CENTER, "%s", text.c_str());
        TCOD_console_printf_ex(con, cw, ch, TCOD_BKGND_DEFAULT, TCOD_CENTER, "%s", res.c_str());
        TCOD_console_flush();
        auto key = TCOD_console_wait_for_keypress(true);
        if (key.pressed) {
            switch (key.vk) {
                case TCODK_ENTER:
                case TCODK_KPENTER:
                    return res;
                case TCODK_ESCAPE:
                    return "";
                case TCODK_BACKSPACE:
                    if (!res.empty()) {
                        res.pop_back();
                    }
                    break;
                case TCODK_KPDEC:
                    res.push_back('.');
                    break;
                case TCODK_KP0:
                case TCODK_KP1:
                case TCODK_KP2:
                case TCODK_KP3:
                case TCODK_KP4:
                case TCODK_KP5:
                case TCODK_KP6:
                case TCODK_KP7:
                case TCODK_KP8:
                case TCODK_KP9:
                    res.push_back(static_cast<char>(key.vk - TCODK_KP0 + '0'));
                    break;
                default:
                    if (key.c != '\0') {
                        if (key.shift) {
                            res.push_back(static_cast<char>(std::toupper(key.c)));
                        } else {
                            res.push_back(key.c);
                        }
                    }
            }
        }
    }
    return "";
}

void ShowMenu(TState& state, const std::vector<TMenuItem>& items, const std::string& title, bool blockExit) {
    auto con = state.Con;
    int idx = 0;
    auto bac_col = TCOD_console_get_default_background(con);
    auto for_col = TCOD_console_get_default_foreground(con);
    while(state.isValid()) {
        TCOD_console_clear(con);

        int width = TCOD_console_get_width(con);
        int height = TCOD_console_get_height(con);
        int ch = (height - 1) / 2;
        int cw = width / 2;
        TCOD_console_printf_ex(con, cw, 0, TCOD_BKGND_DEFAULT, TCOD_CENTER, "%s", title.c_str());
        for (int row = 1; row < height; ++row) {
            int p_id = row - ch + idx;
            if (p_id < 0 || p_id >= items.size()) {
                continue;
            }
            if (p_id == idx) {
                TCOD_console_set_default_background(con, for_col);
                TCOD_console_set_default_foreground(con, bac_col);
                TCOD_console_printf_ex(con, cw, row, TCOD_BKGND_SET, TCOD_CENTER, "%s", items[p_id].Name.c_str());
                TCOD_console_set_default_background(con, bac_col);
                TCOD_console_set_default_foreground(con, for_col);
            } else {
                TCOD_console_printf_ex(con, cw, row, TCOD_BKGND_DEFAULT, TCOD_CENTER, "%s", items[p_id].Name.c_str());
            }
        }
        TCOD_console_flush();
        auto key = TCOD_console_wait_for_keypress(true);
        if (key.pressed) {
            if (key.vk == TCODK_ENTER || key.vk == TCODK_KPENTER) {
                if (idx >= items.size()) {
                    return;
                } else if (items[idx].Func()) {
                    return;
                }
            } else if (key.vk == TCODK_ESCAPE) {
                if (!blockExit) {
                    return;
                }
                
            } else if (items.empty()) {
                continue;
            } else if (key.vk == TCODK_UP) {
                --idx;
                if (idx < 0) {
                    idx = items.size() - 1;
                }
            } else if (key.vk == TCODK_DOWN){
                idx = (idx + 1) % items.size();
            }
        }
    }
}

void ShowTableWithTwoColumn(TState& state, const std::vector<TTableWithTwoColumnItem>& items, const std::string& title, bool blockExit) {
    auto con = state.Con;
    int idx = 0;
    auto bac_col = TCOD_console_get_default_background(con);
    auto for_col = TCOD_console_get_default_foreground(con);
    while(state.isValid()) {
        TCOD_console_clear(con);

        int width = TCOD_console_get_width(con);
        int height = TCOD_console_get_height(con);
        int ch = (height - 1) / 2;
        int cw = width / 2;
        TCOD_console_printf_ex(con, cw, 0, TCOD_BKGND_DEFAULT, TCOD_CENTER, "%s", title.c_str());
        for (int row = 1; row < height; ++row) {
            int p_id = row - ch + idx;
            if (p_id == -1 || p_id == items.size()) {
                TCOD_console_rect(con, 0, row, con->w, 1, true, TCOD_BKGND_DEFAULT);
            }
            if (p_id < 0 || p_id >= items.size()) {
                continue;
            }
            if (p_id == idx) {
                for (int i = 0; i < width; ++i) {
                    TCOD_console_put_char(con, i, row, '.', TCOD_BKGND_DEFAULT);
                }
                TCOD_console_set_default_background(con, for_col);
                TCOD_console_set_default_foreground(con, bac_col);
                TCOD_console_printf_ex(con, 0, row, TCOD_BKGND_SET, TCOD_LEFT, "%s", items[p_id].Left.c_str());
                TCOD_console_printf_ex(con, width - 1, row, TCOD_BKGND_SET, TCOD_RIGHT, "%s", items[p_id].Right.c_str());
                TCOD_console_set_default_background(con, bac_col);
                TCOD_console_set_default_foreground(con, for_col);
            } else {
                TCOD_console_printf_ex(con, 0, row, TCOD_BKGND_DEFAULT, TCOD_LEFT, "%s", items[p_id].Left.c_str());
                TCOD_console_printf_ex(con, width - 1, row, TCOD_BKGND_DEFAULT, TCOD_RIGHT, "%s", items[p_id].Right.c_str());
            }
        }
        TCOD_console_flush();
        auto key = TCOD_console_wait_for_keypress(true);
        if (key.pressed) {
            if (key.vk == TCODK_ENTER) {
                return;
            } else if (key.vk == TCODK_ESCAPE) {
                if (!blockExit) {
                    return;
                }
            } else if (key.vk == TCODK_UP) {
                idx = (idx - 1) % items.size();
            } else if (key.vk == TCODK_DOWN){
                idx = (idx + 1) % items.size();
            }
        }
    }
}

void PrintMap(TState& state, const std::vector<uint8_t>& map, int width, int height, int x, int y, bool printEmpty) {
    auto con = state.Con;
    auto cx = con->w / 2;
    auto cy = con->h / 2;
    auto bc = TCOD_color_RGB(0, 0, 0);
    auto fc = TCOD_color_RGB(255, 255, 255);

    auto exitColor = TCOD_color_RGB(255, 255, 0);
    auto mobColor = TCOD_color_RGB(255, 0, 0);
    for (int i = 0; i < con->w; ++i) {
        for (int j = 0; j < con->h; ++j) {
            auto map_x = i - cx + x;
            auto map_y = j - cy + y;
            if (map_x < 0 || map_y < 0 || map_x >= width || map_y >= height) {
                if (printEmpty) {
                    TCOD_console_put_char_ex(con, i, j, ' ', fc, bc);
                }
                continue;
            }
            int c = static_cast<uint8_t>(map[static_cast<size_t>(map_x + width * map_y)]);
            auto color = fc;
            if (c == '*') {
                color = mobColor;
            } else if (c == TCOD_CHAR_LIGHT) {
                color = exitColor;
            }
            if (c == 0) {
                c = ' ';
                if (printEmpty) {
                    TCOD_console_put_char_ex(con, i, j, c, color, bc);
                }
            } else {
                TCOD_console_put_char_ex(con, i, j, c, color, bc);
            }
        }
    }
}

void MultiplayAllColours(TState& state, float mul) {
    auto con = state.Con;
    for (auto x = 0; x < con->w; ++x) {
        for (auto y = 0; y < con->h; ++y) {
            auto c = TCOD_console_get_char(con, x, y);
            auto fc = TCOD_console_get_char_foreground(con, x, y);
            auto bc = TCOD_console_get_char_background(con, x, y);
            fc = TCOD_color_multiply_scalar(fc, mul);
            bc = TCOD_color_multiply_scalar(bc, mul);
            TCOD_console_put_char_ex(con, x, y, c, fc, bc);
        }
    }
}

void PrintMessage(TState& state, const std::string& msg, TCOD_color_t c) {
    auto con = state.Con;
    auto cx = con->w / 2;
    auto cy = con->h / 2;
    auto for_col = TCOD_console_get_default_foreground(con);
    MultiplayAllColours(state);
    TCOD_console_set_default_foreground(con, c);
    TCOD_console_printf_ex(con, cx, cy, TCOD_BKGND_DEFAULT, TCOD_CENTER, "%s", msg.c_str());
    TCOD_console_set_default_foreground(con, for_col);
    TCOD_console_flush();
    TCOD_console_wait_for_keypress(true);
}
