#include <asio.hpp>

#include "state.h"

class TClient {
    TState& State;
    asio::io_service Service;
    asio::ip::tcp::endpoint Ep;
    asio::ip::tcp::socket Socket;

public:
    TClient(TState& state);
    void Connect(const std::string &host, unsigned short port);
    void Start();
};
