#include "state.h"

TState::TState(TCOD_Console* con) : Con(con) {}

void TState::SetAutorisated() {
    isAutorisated = true;
}

bool TState::isValid() const {
    return !TCOD_console_is_window_closed();
}
