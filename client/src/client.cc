#include <iostream>

#include "client.h"
#include "convector.h"
#include "console.h"

using TSocket = asio::ip::tcp::socket;

TClient::TClient(TState& state)
    : State(state)
    , Socket(Service)
{}

enum class EAuth {
    Registration,
    Autorisation,
};

void PrintUnknownResponse(TState& state, uint16_t key) {
    PrintMessage(state, "Unknown server response: " + std::to_string(key), {255, 0, 0});
}

bool GetLeaderBoard(TState& state, TSocket& socket) {
    SendUi16(socket, 12);
    auto key = GetUi16(socket);
    if (key != 13) {
        PrintUnknownResponse(state, key);
        return false;
    }
    auto count = GetUi8(socket);
    std::vector<TTableWithTwoColumnItem> items;
    items.reserve(count);
    for (size_t i = 0; i < count; ++i) {
        auto name = GetString(socket);
        auto wins = GetUi64(socket);
        /* auto defeats = */ GetUi64(socket);
        /* auto totalGames = */ GetUi64(socket);
        items.push_back(TTableWithTwoColumnItem{std::move(name), std::to_string(wins)});
    }
    ShowTableWithTwoColumn(state, items, "LeaderBoard");
    return false;
}

bool CheckGameKey(TState& state, uint16_t key) {
    switch (key) {
        case 29:
            PrintMessage(state, "You're WIN");
            return true;
        case 30:
            PrintMessage(state, "You're LOSE");
            return true;
        case 31:
            PrintMessage(state, "Game have already finished");
            return true;
        default:
            PrintUnknownResponse(state, key);
            return true;
    }
}

enum class EMoveDirection {
    Up,
    Right,
    Down, 
    Left,
};

bool DoMove(TState& state, TSocket& sock, EMoveDirection dir) {
    SendUi16(sock, 39);
    switch(dir) {
        case EMoveDirection::Up:
            SendUi8(sock, 0);
            break;
        case EMoveDirection::Right:
            SendUi8(sock, 1);
            break;
        case EMoveDirection::Left:
            SendUi8(sock, 2);
            break;
        case EMoveDirection::Down:
            SendUi8(sock, 3);
            break;
    }
    auto key = GetUi16(sock);
    switch (key) {
        case 36:
            PrintMessage(state, "It's not you step");
            return false;
        case 40:
            return false;
        case 41:
            PrintMessage(state, "You can't move in this direction");
            return false;
        default:
            CheckGameKey(state, key);
            return true;
    }
}

bool InGame(TState& state, TSocket& sock, const std::string& roomName) {
    std::vector<uint8_t> map;
    uint16_t width, height;
    uint16_t x, y;

    SendUi16(sock, 32);
    auto key = GetUi16(sock);
    if (key != 33) {
        CheckGameKey(state, key);
        return true;
    } else {
        width = GetUi16(sock);
        height = GetUi16(sock);
        map.resize(static_cast<size_t>(width) * height, 0);
    }
    auto con = state.Con;
    bool playing = true;
    while (state.isValid() && playing) {
        SendUi16(sock, 37);
        auto key = GetUi16(sock);
        if (key != 38) {
            CheckGameKey(state, key);
            return true;
        } else {
            auto lives = GetUi32(sock);
            auto ammunitions = GetUi32(sock);
            x = GetUi16(sock);
            y = GetUi16(sock);
            int widthView = GetUi16(sock);
            int heightView = GetUi16(sock);
            auto mapUpdate = GetRaw(sock, static_cast<size_t>(widthView) * static_cast<size_t>(heightView));
            TCOD_console_clear(con);
            PrintMap(state, map, width, height, x, y, true);
            MultiplayAllColours(state);
            PrintMap(state, mapUpdate, widthView, heightView, widthView / 2, heightView / 2, false);
            TCOD_console_put_char_ex(con, con->w / 2, con->h / 2, '@', {255, 255, 255}, {0, 0, 0});
            TCOD_console_printf_ex(con, con->w - 1, con->h - 1, TCOD_BKGND_DEFAULT, TCOD_RIGHT, "r: refresh");
            TCOD_console_printf_ex(con, con->w - 1, con->h - 2, TCOD_BKGND_DEFAULT, TCOD_RIGHT, "Ammos: %d", ammunitions);
            TCOD_console_printf_ex(con, con->w - 1, con->h - 3, TCOD_BKGND_DEFAULT, TCOD_RIGHT, "Lives: %d", lives);
            TCOD_console_flush();
            for (ssize_t i = 0; i < widthView; ++i) {
                for (ssize_t j = 0; j < heightView; ++j) {
                    auto map_x = i - widthView / 2 + x;
                    auto map_y = j - heightView / 2 + y;
                    if (map_x >= 0 && map_x < width && map_y >= 0 && map_y < height) {
                        auto c = mapUpdate[i + j * widthView];
                        if (c != 0) {
                            map[map_x + map_y * width] = c;
                        }
                    }
                }
            }
        }
        auto userKey = TCOD_console_wait_for_keypress(true);
        if (userKey.pressed) {
            if (userKey.c == 'r') {
                continue;
            }
            if (userKey.vk == TCODK_UP) {
                if (DoMove(state, sock, EMoveDirection::Up)) {
                    return true;
                }
                continue;
            } 
            if (userKey.vk == TCODK_LEFT) {
                if (DoMove(state, sock, EMoveDirection::Left)) {
                    return true;
                }
                continue;
            }
            if (userKey.vk == TCODK_DOWN) {
                if (DoMove(state, sock, EMoveDirection::Down)) {
                    return true;
                }
                continue;
            }
            if (userKey.vk == TCODK_RIGHT) {
                if (DoMove(state, sock, EMoveDirection::Right)) {
                    return true;
                }
                continue;
            }
        }
    }
    return true;
}

bool ShowRoommates(TState& state, TSocket& sock, const std::string& roomName) {
    SendUi16(sock, 20);
    auto key = GetUi16(sock);
    if (key == 21) {
        auto list = GetListString(sock);
        std::vector<TTableWithTwoColumnItem> items;
        items.reserve(list.size());
        for (auto& user : list) {
            items.emplace_back(TTableWithTwoColumnItem{std::move(user), ""});
        }
        ShowTableWithTwoColumn(state, items, "Roommates");
        return false;
    } else if (key == 26) {
        PrintMessage(state, "The room was canceled");
        return true;
    } else if (key == 27) {
        InGame(state, sock, roomName);
        return true;
    } else {
        PrintUnknownResponse(state, key);
        return false;
    }
}

bool CancelRoom(TState& state, TSocket& sock, const std::string& roomMate) {
    SendUi16(sock, 18);
    auto key = GetUi16(sock);
    if (key == 0) {
        PrintMessage(state, "You canceled room");
        return true;
    } else if (key == 23) {
        PrintMessage(state, "You can't canceled room. Try exit");
        return false;
    } else if (key == 26) {
        PrintMessage(state, "Room canceled");
        return true;
    } else if (key == 27) {
        InGame(state, sock, roomMate);
        return true;
    } else {
        PrintUnknownResponse(state, key);
        return false;
    }
}

bool ExitRoom(TState& state, TSocket& sock, const std::string& roomMate) {
    SendUi16(sock, 24);
    auto key = GetUi16(sock);
    if (key == 0) {
        PrintMessage(state, "You exited");
        return true;
    } else if (key == 25) {
        PrintMessage(state, "You're creator and can't exit room. Try cancel");
        return false;
    } else if (key == 26) {
        PrintMessage(state, "Room canceled");
        return true;
    } else if (key == 27) {
        InGame(state, sock, roomMate);
        return true;
    } else {
        PrintUnknownResponse(state, key);
        return false;
    }
}

bool StartRoom(TState& state, TSocket& sock, const std::string& roomMate) {
    SendUi16(sock, 19);
    auto key = GetUi16(sock);
    if (key == 0) {
        InGame(state, sock, roomMate);
        return true;
    } else if (key == 22) {
        PrintMessage(state, "You're can't start room");
        return false;
    } else if (key == 26) {
        PrintMessage(state, "Room canceled");
        return true;
    } else if (key == 27) {
        InGame(state, sock, roomMate);
        return true;
    } else {
        PrintUnknownResponse(state, key);
        return false;
    }
}

bool RefreshStateRoom(TState& state, TSocket& sock, const std::string& roomName) {
    SendUi16(sock, 28);
    auto key = GetUi16(sock);
    if (key == 0) {
        PrintMessage(state, "Refreshed");
        return false;
    } else if (key == 26) {
        PrintMessage(state, "Room canceled");
        return true;
    } else if (key == 27) {
        InGame(state, sock, roomName);
        return true;
    } else {
        return false;
        PrintUnknownResponse(state, key);
    }
}

bool WaitingRoom(TState& state, TSocket& sock, const std::string& roomName) {
    ShowMenu(
        state,
        {
            {"Roommates", [&](){return ShowRoommates(state, sock, roomName);}},
            {"Cancel room. Creator only", [&](){return CancelRoom(state, sock, roomName);}},
            {"Exit room. Non creator only", [&](){return ExitRoom(state, sock, roomName);}},
            {"Start room. Creator only", [&](){return StartRoom(state, sock, roomName);}},
            {"Refresh", [&](){return RefreshStateRoom(state, sock, roomName);}},
        },
        "Succes intering in room: " + roomName,
        /* blockExit = */ true
    );
    return false;
}

bool GetStatistics(TState& state, TSocket& sock) {
    SendUi16(sock, 10);
    auto key = GetUi16(sock);
    if (key != 11) {
        PrintUnknownResponse(state, key);
        return false;
    }
    auto wins = GetUi64(sock);
    auto defeats = GetUi64(sock);
    auto totalGames = GetUi64(sock);
    ShowTableWithTwoColumn(
        state,
        {
            {"Wins", std::to_string(wins)},
            {"Defeats", std::to_string(defeats)},
            {"Total games", std::to_string(totalGames)},
        },
        "Statistics"
    );
    return false;
}

bool CreateRoom(TState& state, TSocket& sock) {
    auto roomName = getInput(state, "Insert room name");
    if (roomName.empty()) {
        return false;
    }
    auto width = std::stoi(getInput(state, "Insert width"));
    if (width <= 0 || width > std::numeric_limits<uint16_t>::max()) {
        ShowMenu(state, {}, "Invalid number");
        return false;
    }
    auto height = std::stoi(getInput(state, "Insert height"));
    if (height <= 0 || height > std::numeric_limits<uint16_t>::max()) {
        ShowMenu(state, {}, "Invalid number");
        return false;
    }
    SendUi16(sock, 14);
    SendString(sock, roomName);
    SendUi16(sock, static_cast<uint16_t>(width));
    SendUi16(sock, static_cast<uint16_t>(height));
    auto key = GetUi16(sock);
    switch (key) {
        case 15:
            return WaitingRoom(state, sock, roomName);
        case 16:
            PrintMessage(state, "Incorrect room size");
            return false;
        case 17:
            PrintMessage(state, "Room with that name already created");
            return false;
        default:
            PrintUnknownResponse(state, key);
            return false;
    }
}

void ConnectToRoom(TState& state, TSocket& sock, const std::string& roomName) {
    SendUi16(sock, 8);
    SendString(sock, roomName);
    auto key = GetUi16(sock);
    if (key == 0) {
        InGame(state, sock, roomName);
    } else if (key == 9) {
        PrintMessage(state, "You're not in list");
    } else if (key == 15) {
        WaitingRoom(state, sock, roomName);
    } else if (key == 404) {
        PrintMessage(state, "Room not found");
    } else {
        PrintUnknownResponse(state, key);
    }
}

bool GetRooms(TState& state, TSocket& sock) {
    SendUi16(sock, 4);
    if (auto key = GetUi16(sock) != 5) {
        PrintUnknownResponse(state, key);
        return false;
    }
    auto waiting = GetListString(sock);
    auto gaming = GetListString(sock);
    std::vector<TMenuItem> items;
    for (const auto& room : gaming) {
        items.emplace_back("[G] " + room, [&](){
            ConnectToRoom(state, sock, room);
            return true;
        });
    }
    for (const auto& room : waiting) {
        items.emplace_back("[W] " + room, [&](){
            ConnectToRoom(state, sock, room);
            return true;
        });
    }
    ShowMenu(state, items, "Rooms");
    return false;
}

bool MainServerMenu(TState& state, TSocket& sock, const std::string& user) {
    bool res = false;

    ShowMenu(
        state,
        {
            {"LeaderBoard", [&](){return GetLeaderBoard(state, sock);}},
            {"Statistics", [&](){return GetStatistics(state, sock);}},
            {"Create room", [&](){return CreateRoom(state, sock);}},
            {"Get rooms", [&](){return GetRooms(state, sock);}},
            {"Sign Out", [](){return true;}},
            {"Quit", [&](){
                res = true;
                return true;
            }},
        },
        "Hello: " + user
    );

    return res;
}

bool LogSign(TState& state, TSocket& socket, EAuth auth) {
    auto login = getInput(state, "Insert login");
    if (login.empty()) {
        return false;
    }
    auto password = getInput(state, "Insert password");
    if (password.empty()) {
        return false;
    }
    SendUi16(socket, auth == EAuth::Registration ? 6 : 1);
    SendString(socket, login);
    SendString(socket, password);
    auto ret = GetUi16(socket);
    if (ret == 0) {
        state.SetAutorisated();
        return MainServerMenu(state, socket, login);
    } else if (ret == 7) {
        PrintMessage(state, "User with this namer already registered");
        return false;
    } else if (ret == 2) {
        PrintMessage(state, "Incorrect user and password");
        return false;
    } else {
        PrintUnknownResponse(state, ret);
        return false;
    }
}

void TClient::Connect(const std::string &host, unsigned short port) {
    try {
        Ep.address(asio::ip::make_address(host));
        Ep.port(port);
        Socket.connect(Ep);
        {
            auto key = GetUi16(Socket);
            assert(key == 0);
        }
        SendUi16(Socket, 256);
        {
            auto key = GetUi16(Socket);
            assert(key == 257);
        }
        auto serverName = GetString(Socket);
        ShowMenu(
            State,
            {
                {"Registration", [&](){return LogSign(State, Socket, EAuth::Registration);}},
                {"Autorisation", [&](){return LogSign(State, Socket, EAuth::Autorisation);}},
                {"LeaderBoard", [&](){return GetLeaderBoard(State, Socket);}},
                {"Quit", [](){return true;}},
            },
            "Welcom to: " + serverName
        );
        PrintMessage(State, "Goodbye!!");
    } catch (...) {
        PrintMessage(State, "Error", {255, 0, 0});
    }
}
