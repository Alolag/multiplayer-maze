#include <libtcod/libtcod.hpp>
#include <cmath>

#include "room.h"

TMemberGameInfo::TMemberGameInfo(const PMemberGameInfo& user)
    : Name(user.name())
    , X(static_cast<uint16_t>(user.x()))
    , Y(static_cast<uint16_t>(user.y()))
    , Lives(user.lives())
    , Ammunitions(user.ammunitions())
    , ViewDistance(static_cast<uint8_t>(user.viewdistance()))
    , AmmunitionsDistance(static_cast<uint8_t>(user.ammunitionsdistance()))
    , IsAlive(user.isalive())
    , IsMonster(user.ismonster())
{}

TMemberGameInfo::TMemberGameInfo(const std::string& name)
    : Name(name)
    , IsMonster(false)
{}

bool TMemberGameInfo::operator==(const std::string& user) const {
    return Name == user;
}

void TMemberGameInfo::Save(PMemberGameInfo* user) const {
    user->set_name(Name);
    user->set_x(X);
    user->set_y(Y);
    user->set_lives(Lives);
    user->set_ammunitions(Ammunitions);
    user->set_viewdistance(ViewDistance);
    user->set_ammunitionsdistance(AmmunitionsDistance);
    user->set_isalive(IsAlive);
    user->set_ismonster(IsMonster);
}

TRoom::TRoom(const PRoom& room)
    : Name(room.name())
    , CreatorName(room.creatorname())
    , Step(room.step())
    , Width(static_cast<uint16_t>(room.width()))
    , Height(static_cast<uint16_t>(room.height()))
    , Map(room.map())
    , FovMap(Width, Height)
    , IsStarted(room.isstarted())
{
    Members.reserve(static_cast<size_t>(room.members_size()));
    for (int i = 0; i < room.members_size(); ++i) {
        Members.emplace_back(room.members(i));
    }
}

TRoom::TRoom(const std::string& name, const std::string& creatorName, uint16_t width, uint16_t height)
    : Name(name)
    , CreatorName(creatorName)
    , Width(width)
    , Height(height)
    , FovMap(Width, Height)
    , IsStarted(false)
{}

void TRoom::Save(PRoom* room) {
    boost::shared_lock<boost::shared_mutex> lock(Mutex);
    room->set_name(Name);
    room->set_creatorname(CreatorName);
    room->set_isstarted(IsStarted);
    room->set_step(Step);
    room->set_width(Width);
    room->set_height(Height);
    room->set_map(Map);
    for (const auto& user : Members) {
        user.Save(room->add_members());
    }
}

void TRoom::GenerateFovMap() {
    FovMap.clear();
    for (size_t x = 0; x < Width; ++x) {
        for (size_t y = 0; y < Height; ++y) {
            if (Map[x + Width * y] == static_cast<char>(TCOD_CHAR_BLOCK1)) {
                FovMap.setProperties(x, y, true, true);
            }
        }
    }
}

bool TRoom::HasMember(const std::string& name) {
    auto ptr = std::find(Members.begin(), Members.end(), name);
    return ptr != Members.end();
}

void TRoom::AddMember(std::string user) {
    Members.emplace_back(std::move(user));
}

class TBSPDigging : public ITCODBspCallback {
    const uint16_t Width;
    const uint16_t Height;
    std::string& Map;
    TCODRandom Rnd;

public:
    TBSPDigging(uint16_t width, uint16_t height, std::string& map)
        : Width(width)
        , Height(height)
        , Map(map)
        , Rnd()
    {}

    char& GetTile(size_t x, size_t y) {
        return Map[x + Width * y];
    }

    void SetTile(size_t x, size_t y, TCOD_chars_t c) {
        GetTile(x, y) = static_cast<char>(c);
    }

    void DigRectangle(size_t x, size_t y, size_t width, size_t height) {
        for (auto i = x; i < x + width; ++i) {
            for (auto j = y; j < y + height; ++j) {
                SetTile(i, j, TCOD_CHAR_BLOCK1);
            }
        }
    }

    void DigRoom(const TCODBsp& leaf) {
        auto rx = Rnd.get(0, leaf.w / 3);
        auto ry = Rnd.get(0, leaf.h / 3);
        size_t x = static_cast<size_t>(leaf.x + rx);
        size_t y = static_cast<size_t>(leaf.y + ry);
        size_t w = static_cast<size_t>(leaf.w - rx); 
        size_t h = static_cast<size_t>(leaf.h - ry); 
        w -= static_cast<size_t>(Rnd.get(0, leaf.w / 3));
        h -= static_cast<size_t>(Rnd.get(0, leaf.h / 3));
        DigRectangle(x, y, w, h);
    }

    bool visitNode(TCODBsp *node, void *) override {
        if (node->isLeaf()) {
            DigRoom(*node);
        } else {
            DigPath(*node);
        }
        return true;
    }

    void DigPath(const TCODBsp& node) {
        auto left = node.getLeft();
        auto right = node.getRight();
        if (left == nullptr || right == nullptr) {
            return;
        }
        auto cx1 = static_cast<size_t>(left->x + left->w / 2);
        auto cy1 = static_cast<size_t>(left->y + left->h / 2);
        auto cx2 = static_cast<size_t>(right->x + right->w / 2);
        auto cy2 = static_cast<size_t>(right->y + right->h / 2);
        if (cx1 == cx2) {
            if (cy1 > cy2) {
                std::swap(cy1, cy2);
            }
            auto h = cy2 - cy1;
            DigRectangle(cx1, cy1, 1, h);
        } else {
            if (cx1 > cx2) {
                std::swap(cx1, cx2);
            }
            auto w = cx2 - cx1;
            DigRectangle(cx1, cy1, w, 1);
        }
    }
};

void TRoom::GenerateMap() {
    Map.resize(static_cast<size_t>(Width) * static_cast<size_t>(Height), 0);
    int numIteration = static_cast<int>(std::ceil((Width / 20.) * (Height / 20.)));
    auto bsp = TCODBsp{1, 1, Width - 1, Height - 1};
    bsp.splitRecursive(nullptr, numIteration, 5, 5, 1.5, 1.5);
    auto digging = TBSPDigging(Width, Height, Map);
    bsp.traversePreOrder(&digging, nullptr);
    GenerateFovMap();
}

struct TCoordinate {
    uint16_t X;
    uint16_t Y;
    char& C;
};

void TRoom::FillMembersCoordinate() {
    TCODRandom rnd;
    std::vector<TCoordinate> freeSpace;
    freeSpace.reserve(Map.size());
    for (size_t x = 1; x < Width - 1; ++x) {
        for (size_t y = 1; y < Height - 1; ++y) {
            if (Map[x + y * Width] == static_cast<char>(TCOD_CHAR_BLOCK1)) {
                freeSpace.push_back({
                    static_cast<uint16_t>(x),
                    static_cast<uint16_t>(y),
                    Map[x + y * Width]
                });
            }
        }
    }
    for (auto& member : Members) {
        size_t x(rnd.getInt(0, freeSpace.size() - 1));
        while(freeSpace[x].C != static_cast<char>(TCOD_CHAR_BLOCK1)) {
            x = rnd.getInt(0, freeSpace.size() - 1);
        }
        member.X = freeSpace[x].X;
        member.Y = freeSpace[x].Y;
        freeSpace[x].C = '*';
    }
    size_t x(rnd.getInt(0, freeSpace.size() - 1));
    while(freeSpace[x].C != static_cast<char>(TCOD_CHAR_BLOCK1)) {
        x = rnd.getInt(0, freeSpace.size() - 1);
    }
    freeSpace[x].C = TCOD_CHAR_LIGHT;
}

void printMap(const std::string& map, size_t width, size_t height) {
    for (size_t i = 0; i < width; ++i) {
        for (size_t j = 0; j < height; ++j) {
            if (map[i + j * width] == 0) {
                std::cout.put(' ');
            } else {
                std::cout.put('#');
            }
        }
        std::cout << std::endl;
    }
}

void TRoom::Start(std::function<void()> func) {
    boost::lock_guard<boost::shared_mutex> lock(Mutex);
    IsStarted = true;
    GenerateMap();
    FillMembersCoordinate();
    func();
}

std::pair<uint16_t, uint16_t> TRoom::GetCoordinate(std::string::iterator it) {
    auto t = std::distance(Map.begin(), it);
    uint16_t x = static_cast<uint16_t>(t / Width);
    uint16_t y = static_cast<uint16_t>(t % Width);
    return {x, y};
}

std::vector<std::string> TRoom::GetMembersName() {
    boost::shared_lock<boost::shared_mutex> lock(Mutex);
    std::vector<std::string> res;
    res.reserve(Members.size());
    for (const auto& user : Members) {
        res.emplace_back(user.Name);
    }
    return res;
}

std::vector<TMemberGameInfo>& TRoom::GetMembers() {
    return Members;
}

const std::string& TRoom::GetCreatorName() {
    return CreatorName;
}

boost::shared_mutex& TRoom::GetMutex() {
    return Mutex;
}

bool TRoom::GetIsStarted() {
    return IsStarted;
}

bool TRoom::DeleteMember(const std::string& name) {
    boost::lock_guard<boost::shared_mutex> lock(Mutex);
    if (name == CreatorName) {
        return false;
    }
    auto it = std::find(
        Members.begin(),
        Members.end(),
        name
    );
    if (it == Members.end()) {
        return true;
    }
    Members.erase(it);
    return true;
}

const std::string& TRoom::GetName() {
    return Name;
}

std::pair<uint16_t, uint16_t> TRoom::GetRoomSize() {
    return {Width, Height};
}

bool TRoom::IsUserStep(const std::string& name) {
    boost::lock_guard<boost::shared_mutex> lock(Mutex);
    return Members[Step].Name == name;
}

TUserState TRoom::GetUserState(const std::string& user, bool usingCoordinate) {
    boost::shared_lock<boost::shared_mutex> lock(Mutex);
    TUserState state;
    auto it = std::find(Members.begin(), Members.end(), user);
    if (it == Members.end()) {
        return state;
    }
    state.UserNotFound = false;
    if (usingCoordinate) {
        state.HasCoordinate = true;
        state.X = it->X;
        state.Y = it->Y;
    }
    state.Lives = it->Lives;
    state.Ammunitions = it->Ammunitions;
    ssize_t vd = it->ViewDistance;
    ssize_t size = 1 + 2 * it->ViewDistance;
    state.WidthView = size;
    state.HeightView = size;
    state.View.resize(static_cast<size_t>(size * size), 0);
    FovMap.computeFov(it->X, it->Y, vd);
    for (ssize_t i = 0; i < size; ++i) {
        auto x = it->X - vd + i;
        if (x < 0 || x >= Width) {
            continue;
        }
        for (ssize_t j = 0; j < size; ++j) {
            auto y = it->Y - vd + j;
            if (y < 0 || y >= Height) {
                continue;
            }
            if (FovMap.isInFov(x, y)) {
                state.View[static_cast<size_t>(i + j * size)] =
                    Map[static_cast<size_t>(x + y * Width)];
            }
        }
    }
    return state;
}

EMoveResult TRoom::DoMove(const std::string& name, EMoveDirection dir) {
    boost::lock_guard<boost::shared_mutex> lock(Mutex);
    auto it = std::find(Members.begin(), Members.end(), name);
    if (it == Members.end()) {
        return EMoveResult::UserNotFound;
    }
    if (Members[Step].Name != name) {
        return EMoveResult::NotYouStep;
    }
    ssize_t x = it->X;
    ssize_t y = it->Y;
    switch (dir) {
        case EMoveDirection::Up:
            --y;
            break;
        case EMoveDirection::Left:
            --x;
            break;
        case EMoveDirection::Right:
            ++x;
            break;
        case EMoveDirection::Down:
            ++y;
            break;
    }
    if (y < 0 || y >= Height || x < 0 || x >= Width) {
        return EMoveResult::NotMoved;
    }
    auto& tile = Map[x + y * static_cast<size_t>(Width)];
    if (tile == 0) {
        return EMoveResult::NotMoved;
    }
    if (tile == static_cast<char>(TCOD_CHAR_BLOCK1)) {
        tile = '*';
        Map[it->X + it->Y * static_cast<size_t>(Width)] = static_cast<char>(TCOD_CHAR_BLOCK1);
        it->X = x;
        it->Y = y;
    } else if (tile == '*') {
        auto vs = std::find_if(Members.begin(), Members.end(), [&](const TMemberGameInfo& u){
            return x == u.X && y == u.Y;
        });
        if (vs == Members.end()) {
            tile = '*';
            Map[it->X + it->Y * static_cast<size_t>(Width)] = static_cast<char>(TCOD_CHAR_BLOCK1);
            it->X = x;
            it->Y = y;
        } else {
            if (it->Ammunitions > 0) {
                --it->Ammunitions;
                --vs->Lives;
                if (vs->Lives == 0) {
                    vs->Name.clear();
                    vs->IsAlive = false;
                    tile = '*';
                    Map[it->X + it->Y * static_cast<size_t>(Width)] = static_cast<char>(TCOD_CHAR_BLOCK1);
                    it->X = x;
                    it->Y = y;
                }
            }
        }
    } else if (tile == static_cast<char>(TCOD_CHAR_LIGHT)) {
        return EMoveResult::Win;
    }
    ++Step;
    FixStep();
    if (AiStep()) {
        return EMoveResult::AllHumansKilled;
    }
    return EMoveResult::Moved;
}

void TRoom::FixStep() {
    if (Step >= Members.size()) {
        Step = 0;
    }
}

bool TRoom::AiStep() {
    bool doSmth = false;
    const auto aiStepStart = Step;
    do{
        auto& mem = Members[Step];
        if (mem.IsAlive && !mem.IsMonster) {
            break;
        }
        if (mem.IsAlive && mem.IsMonster) {
            //TODO Monster Step
        }
        doSmth = true;
        ++Step;
        FixStep();
    } while (Step != aiStepStart);
    return aiStepStart == Step && doSmth;
}
