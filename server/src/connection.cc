#include "convector.h"
#include "connection.h"

#include <iostream>

TConnection::TConnection(TState& state, asio::io_service& service, asio::ip::tcp::acceptor& acceptor, boost::thread_group& threadsGroup)
    : State(state)
    , Service(service)
    , Sock(Service)
    , Acceptor(acceptor)
    , ThreadsGroup(threadsGroup)
{
    State.AddConnections(this);
}

TConnection::~TConnection() {
    State.DeleteConnection(this);
}

TConnection::ptr TConnection::New(TState& state, asio::io_service& service, asio::ip::tcp::acceptor& acceptor, boost::thread_group& threadsGroup) {
    return ptr(new TConnection(state, service, acceptor, threadsGroup));
}

TConnection::ptr TConnection::New() const {
    return New(State, Service, Acceptor, ThreadsGroup);
}

void TConnection::LogSign(EAuth auth) {
    auto login = GetString(Sock);
    auto password = GetString(Sock);
    switch (auth) {
        case EAuth::Registration:
            if (State.AddUser(login, password)) {
                User = login;
                SendUi16(Sock, 0);
            } else {
                SendUi16(Sock, 7);
            }
            break;
        case EAuth::Autorisation:
            if (State.CheckUser(login, password)) {
                User = login;
                SendUi16(Sock, 0);
            } else {
                SendUi16(Sock, 2);
            }
            break;
    }
}

void TConnection::SendRoomsLists() {
    auto  roomsPair = State.GetRoomsLists(User);
    SendUi16(Sock, 5);
    SendListString(Sock, roomsPair.first);
    SendListString(Sock, roomsPair.second);
}

void TConnection::SendLeaderBoard() {
    auto leaders = State.GetLeaderBoard();
    SendUi16(Sock, 13);
    SendUi8(Sock, static_cast<uint8_t>(leaders.size()));
    for (const auto& user : leaders) {
        SendString(Sock, user.Name);
        SendUi64(Sock, user.Wins);
        SendUi64(Sock, user.Defeats);
        SendUi64(Sock, user.TotalGames);
    }
}

void TConnection::CreateRoom() {
    auto roomName = GetString(Sock);
    auto width = GetUi16(Sock);
    auto height = GetUi16(Sock);
    if (!State.CheckRoomSize(width, height)) {
        SendUi16(Sock, 16);
    } else if (State.AddRoom(User, roomName, width, height)) {
        SendUi16(Sock, 15);
        RoomName = roomName;
    } else {
        SendUi16(Sock, 17);
    }
}

void TConnection::GetInRoom() {
    auto roomName = GetString(Sock);
    if (User.empty()) {
        SendUi16(Sock, 253);
        return;
    } else if(!RoomName.empty()) {
        SendUi16(Sock, 254);
        return;
    } else if (roomName.empty()) {
        SendUi16(Sock, 3);
        return;
    }
    auto res = State.GetInRoom(User, roomName);
    switch (res){
        case EGetInRoomResult::EnterInGamingRoom:
            RoomName = roomName;
            SendUi16(Sock, 0);
            break;
        case EGetInRoomResult::UserNotInGamingList:
            SendUi16(Sock, 9);
            break;
        case EGetInRoomResult::GetInWaitingRoom:
            RoomName = roomName;
            SendUi16(Sock, 15);
            break;
        case EGetInRoomResult::RoomNotFound:
            SendUi16(Sock, 404);
            break;
    }
}

void TConnection::DoMove() {
    auto dir = GetUi8(Sock);
    if (User.empty()) {
        SendUi16(Sock, 253);
    } else if (RoomName.empty()) {
        SendUi16(Sock, 254);
    } else {
        auto res = State.DoMove(User, RoomName, EMoveDirection(dir));
        switch (res) {
            case EMoveResult::Moved:
                SendUi16(Sock, 40);
                break;
            case EMoveResult::NotMoved:
                SendUi16(Sock, 41);
                break;
            case EMoveResult::NotYouStep:
                SendUi16(Sock, 36);
                break;
            case EMoveResult::UserNotFound:
                RoomName.clear();
                SendUi16(Sock, 30);
                break;
            case EMoveResult::Win:
                RoomName.clear();
                SendUi16(Sock, 29);
                break;
            case EMoveResult::AllHumansKilled:
                RoomName.clear();
                SendUi16(Sock, 30);
                break;
        }
    }
}

void TConnection::Start() {
    IsConnected_ = true;
    SendUi16(Sock, 0);
    while(Sock.is_open()) {
        try {
            auto key = GetUi16(Sock);
            switch (key) {
                case 1:
                    if (!RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        LogSign(EAuth::Autorisation);
                    }
                    break;
                case 4:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if(!RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        SendRoomsLists();
                    }
                    break;
                case 6:
                    if (!RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        LogSign(EAuth::Registration);
                    }
                    break;
                case 8:
                    GetInRoom();
                    break;
                case 10:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if(!RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        auto userInfo = State.GetUserInfo(User);
                        SendUi16(Sock, 11);
                        SendUi64(Sock, userInfo.Wins);
                        SendUi64(Sock, userInfo.Defeats);
                        SendUi64(Sock, userInfo.TotalGames);
                    }
                    break;
                case 12:
                    SendLeaderBoard();
                    break;
                case 14:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if(!RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        CreateRoom();
                    }
                    break;
                case 18:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else if (State.DeleteRoom(User, RoomName)) {
                        SendUi16(Sock, 0);
                        RoomName.clear();
                    } else {
                        SendUi16(Sock, 23);
                    }
                    break;
                case 19:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else if (State.StartRoom(User, RoomName)) {
                        SendUi16(Sock, 0);
                    } else {
                        SendUi16(Sock, 22);
                    }
                    break;
                case 20:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        SendUi16(Sock, 21);
                        SendListString(Sock, State.GetMembersList(RoomName));
                    }
                    break;
                case 24:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else if (State.DeleteMember(User, RoomName)) {
                        SendUi16(Sock, 0);
                        RoomName.clear();
                    } else {
                        SendUi16(Sock, 25);
                    }
                    break;
                case 28:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        auto res = State.GetRoomState(RoomName);
                        switch (res) {
                            case ERoomState::Canceled:
                                RoomName.clear();
                                SendUi16(Sock, 26);
                                break;
                            case ERoomState::Gaming:
                                SendUi16(Sock, 27);
                                break;
                            case ERoomState::Waiting:
                                SendUi16(Sock, 0);
                                break;
                        }
                    }
                    break;
                case 32:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        auto pair = State.GetRoomSize(RoomName);
                        if (pair == std::make_pair<uint16_t, uint16_t>(0, 0)) {
                            SendUi16(Sock, 31);
                        } else {
                            SendUi16(Sock, 33);
                            SendUi16(Sock, pair.first);
                            SendUi16(Sock, pair.second);
                        }
                    }
                    break;
                case 34:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        auto res = State.IsUserStep(User, RoomName);
                        switch (res) {
                            case EUserStep::RoomNotFound:
                                SendUi16(Sock, 31);
                                RoomName.clear();
                                break;
                            case EUserStep::UsersStep:
                                SendUi16(Sock, 35);
                                break;
                            case EUserStep::NotThatUserStep:
                                SendUi16(Sock, 36);
                                break;
                        }
                    }
                    break;
                case 37:
                    if (User.empty()) {
                        SendUi16(Sock, 253);
                    } else if (RoomName.empty()) {
                        SendUi16(Sock, 254);
                    } else {
                        auto res = State.GetUserState(User, RoomName);
                        if (res.UserNotFound) {
                            SendUi16(Sock, 31);
                            RoomName.clear();
                        } else if (res.HasCoordinate) {
                            SendUi16(Sock, 38);
                            SendUi32(Sock, res.Lives);
                            SendUi32(Sock, res.Ammunitions);
                            SendUi16(Sock, res.X);
                            SendUi16(Sock, res.Y);
                            SendUi16(Sock, res.WidthView);
                            SendUi16(Sock, res.HeightView);
                            SendRawString(Sock, res.View);
                        }
                    }
                    break;
                case 39:
                    DoMove();
                    break;
                case 256:
                    SendUi16(Sock, 257);
                    SendString(Sock, State.SrverName);
                    break;
                default:
                    SendUi16(Sock, 255);
                    break;
            }
        } catch (std::system_error a) {
            if (a.code() == asio::error::eof || a.code() == asio::error::connection_reset) {
                Sock.close();
            } else {
                std::cout << a.code() << std::endl;
            }
        }
    }
    std::cout << "Client disconected" << std::endl;
}

asio::ip::tcp::acceptor& TConnection::GetAcceptor() {
    return Acceptor;
}

asio::ip::tcp::socket& TConnection::GetSock() {
    return Sock;
}

const std::string& TConnection::GetName() const {
    return User;
}

boost::thread_group& TConnection::GetThreadsGrop() {
    return ThreadsGroup;
}

bool TConnection::IsConnected() const {
    return IsConnected_;
}
