#include <asio.hpp>
#include <boost/thread.hpp>

#include "state.h"

class TServer {
    TState &State;
    asio::io_service Service;
    asio::ip::tcp::acceptor Acceptor;
    boost::thread_group ThreadsGroup;

public:
    TServer(TState&, unsigned short);
    ~TServer();

    void Stop();
    void Start();
};
