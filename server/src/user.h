#include <string>
#include <cstdint>

#include <state.pb.h>

class TUser {
    const std::string Name;
    const std::string Password;
    uint64_t Wins = 0;
    uint64_t Defeats = 0;
    uint64_t TotalGames = 0;

public:
    TUser(const PUser& in);
    TUser(const std::string& name, const std::string& password);
    bool CheckPassowrd(const std::string& password);
    void Save(PUser*) const;
    const std::string& GetName() const;
    uint64_t& GetWins();
    uint64_t& GetDefeats();
    uint64_t& GetTotalGames();
    const uint64_t& GetWins() const;
    const uint64_t& GetDefeats() const;
    const uint64_t& GetTotalGames() const;
};
