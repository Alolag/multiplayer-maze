#include <algorithm>
#include <experimental/filesystem>
#include <fstream>

#include "state.h"
#include "connection.h"

namespace fs = std::experimental::filesystem;
using shared_lock = boost::shared_lock<boost::shared_mutex>;
using lock_guard = boost::lock_guard<boost::shared_mutex>;

TState::TState(const PConfig& config)
    : StateFileName(config.statefile())
    , DefaultLives(config.defaultlives())
    , DefaultAmmunitions(config.defaultammunitions())
    , DefaultMonsterCounts(config.defaultmonstercounts())
    , DefaultViewDistance(static_cast<uint8_t>(config.defaultviewdistance()))
    , SrverName(config.servername())
{
    if (fs::exists(StateFileName)) {
        std::ifstream file(StateFileName, std::ios::binary | std::ios::in);
        auto state = PState();
        state.ParseFromIstream(&file);
        std::vector<std::string> users;
        for (auto userIdx = 0; userIdx < state.users_size(); ++userIdx) {
            auto user = state.users(userIdx);
            Users.emplace(user.name(), user);
            users.emplace_back(user.name());
        }
        UpdateLeaderBoard(users);
        for (auto roomIdx = 0; roomIdx < state.rooms_size(); ++roomIdx) {
            auto room = state.rooms(roomIdx);
            Rooms.emplace(room.creatorname(), room);
        }
    }
}

bool TState::AddUser(const std::string& name, const std::string& password) {
    shared_lock globalLock(Globalmutex);
    {
        lock_guard lock(UsersMutex);

        if (Users.find(name) != Users.end()) {
            return false;
        }

        Users.emplace(
            std::piecewise_construct,
            std::forward_as_tuple(name),
            std::forward_as_tuple(name, password)
        );
    }
    UpdateLeaderBoard({name});
    std::cout << "User: " << name << " added" << std::endl;
    return true;
}

bool TState::AddRoom(const std::string& creatorName, const std::string& name, uint16_t width, uint16_t height) {
    shared_lock lock(Globalmutex);
    lock_guard roomsLock(RoomsMutex);
    auto it = Rooms.emplace(
        std::piecewise_construct,
        std::forward_as_tuple(name),
        std::forward_as_tuple(name, creatorName, width, height)
    );
    if (it.second) {
        it.first->second.AddMember(creatorName);
        return true;
    }
    return false;
}

bool TState::CheckUser(const std::string& name, const std::string& password) {
    shared_lock globalLock(Globalmutex);
    shared_lock lock(UsersMutex);

    auto iter = Users.find(name);
    if (iter == Users.end()) {
        return false;
    } else {
        std::cout << "User: " << name << " authotisated" << std::endl;
        return iter->second.CheckPassowrd(password);
    }
}

void TState::AddConnections(TConnection* conn) {
    lock_guard lock(ConnectionsMutex);
    Connections.emplace(conn);
}

void TState::DeleteConnection(TConnection* conn) {
    lock_guard lock(ConnectionsMutex);
    Connections.erase(conn);
}

void TState::ShowConnections() {
    shared_lock lock(ConnectionsMutex);
    for (auto conn : Connections) {
        if (!conn->IsConnected()) {
            continue;
        }
        auto name = conn->GetName();
        if (name.empty()) {
            std::cout << "UnAuthotisated user" << std::endl;
        } else {
            std::cout << "Authotisated user: " << name << std::endl;
        }
    }
}

EGetInRoomResult TState::GetInRoom(const std::string& userName, const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    auto roomIt = Rooms.find(roomName);
    if (roomIt == Rooms.end()) {
        return EGetInRoomResult::RoomNotFound;
    }
    auto& room = roomIt->second;
    auto isUser = room.HasMember(userName);
    lock_guard lock(room.GetMutex());
    if (room.GetIsStarted()) {
        if (isUser) {
            return EGetInRoomResult::EnterInGamingRoom;
        } else {
            return EGetInRoomResult::UserNotInGamingList;
        }
    }
    if (!isUser) {
        room.AddMember(userName);
    }
    return EGetInRoomResult::GetInWaitingRoom;
}

bool TState::DeleteRoom(const std::string& user, const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    lock_guard lock(RoomsMutex);
    auto roomIt = Rooms.find(roomName);
    if (roomIt == Rooms.end()) {
        return true;
    }
    auto& room = roomIt->second;
    if (room.GetCreatorName() == user) {
        Rooms.erase(roomIt);
        return true;
    }
    return false;
}

bool TState::StartRoom(const std::string& user, const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    lock_guard lock(RoomsMutex);
    auto roomIt = Rooms.find(roomName);
    if (roomIt == Rooms.end()) {
        return false;
    }
    auto& room = roomIt->second;
    if (room.GetCreatorName() == user) {
        room.Start([&](){
            lock_guard usersLock(UsersMutex);
            for (auto& gamer : room.GetMembers()) {
                gamer.IsAlive = true;
                if (gamer.IsMonster) {
                    gamer.Lives = 1;
                    gamer.Ammunitions = -1;
                } else {
                    gamer.Ammunitions = DefaultAmmunitions;
                    gamer.Lives = DefaultLives;
                    gamer.ViewDistance = DefaultViewDistance;
                    auto it = Users.find(gamer.Name);
                    if (it == Users.end()) {
                        continue;
                    }
                    ++it->second.GetTotalGames();
                }
            }
        });
        return true;
    }
    return false;
}

EMoveResult TState::DoMove(const std::string& userName, const std::string& roomName, EMoveDirection dir) {
    shared_lock globalLock(Globalmutex);
    lock_guard lock(RoomsMutex);
    auto roomIt = Rooms.find(roomName);
    if (roomIt == Rooms.end()) {
        return EMoveResult::UserNotFound;
    }
    auto res = roomIt->second.DoMove(userName, dir);
    if (res == EMoveResult::Win || res == EMoveResult::AllHumansKilled) {
        lock_guard userLock(UsersMutex);
        std::vector<std::string> newUsers;
        for (auto gamer : roomIt->second.GetMembersName()) {
            auto user = Users.find(gamer);
            if (user != Users.end()) {
                newUsers.push_back(gamer);
                if (userName == gamer && res == EMoveResult::Win) {
                    ++user->second.GetWins();
                } else {
                    ++user->second.GetDefeats();
                }
            }
        }
        Rooms.erase(roomIt);
        UpdateLeaderBoardWithoutMutex(newUsers);
    }
    return res;
}

std::vector<std::string> TState::GetMembersList(const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    auto roomIt = Rooms.find(roomName);
    if (roomIt == Rooms.end()) {
        return {};
    }
    return roomIt->second.GetMembersName();
}

bool TState::DeleteMember(const std::string& user, const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    auto it = Rooms.find(roomName);
    if (it == Rooms.end()) {
        return true;
    }
    return it->second.DeleteMember(user);
}

ERoomState TState::GetRoomState(const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    auto it = Rooms.find(roomName);
    if (it == Rooms.end()) {
        return ERoomState::Canceled;
    }
    if (it->second.GetIsStarted()) {
        return ERoomState::Gaming;
    }
    return ERoomState::Waiting;
}

void TState::Save(std::function<void()> func) {
    std::cout << "Start saving" << std::endl;
    lock_guard globalLock(Globalmutex);
    lock_guard userLock(UsersMutex);
    lock_guard roomsLock(RoomsMutex);
    PState state;

    for (const auto& pair : Users) {
        pair.second.Save(state.add_users());
    }

    for (auto& pair : Rooms) {
        auto room = state.add_rooms();
        pair.second.Save(room);
    }

    std::ofstream file(StateFileName, std::ios::binary | std::ios::trunc | std::ios::out);
    state.SerializeToOstream(&file);
    std::cout << "Finished saving" << std::endl;
    func();
}

std::vector<TSimpleUser> TState::GetLeaderBoard() {
    shared_lock lock(LeaderBoardMutex);
    std::vector<TSimpleUser> res;
    res.reserve(LeaderBoard.size());
    for (const auto& name : LeaderBoard) {
        const auto& user = Users.find(name)->second;
        res.emplace_back(user);
    }
    return res;
}

void TState::UpdateLeaderBoardWithoutMutex(const std::vector<std::string>& users) {
    LeaderBoard.insert(LeaderBoard.end(), users.begin(), users.end());

    auto cmpUsers = [&](const std::string& a, const std::string& b) {
        auto aa = Users.find(a);
        auto bb = Users.find(b);
        return std::tie(aa->second.GetWins(), b) > std::tie(bb->second.GetWins(), a);
    };

    std::sort(
        LeaderBoard.begin(),
        LeaderBoard.end(),
        cmpUsers
    );
    auto last = std::unique(
        LeaderBoard.begin(),
        LeaderBoard.end()
    );
    LeaderBoard.erase(last, LeaderBoard.end());
    LeaderBoard.resize(std::min<size_t>(LeaderBoard.size(), 10));
}

void TState::UpdateLeaderBoard(const std::vector<std::string>& users) {
    shared_lock globalLock(Globalmutex);
    lock_guard leaderBoardLock(LeaderBoardMutex);
    shared_lock usersLock(UsersMutex);

    UpdateLeaderBoardWithoutMutex(users);
}

bool TState::CheckRoomSize(uint16_t width, uint16_t height) const {
    return width > 10 && width < 1000 && height > 10 && height < 1000;
}

TSimpleUser TState::GetUserInfo(const std::string& name) {
    shared_lock globalLock(Globalmutex);
    shared_lock usersLock(UsersMutex);
    auto it = Users.find(name);
    auto res = TSimpleUser();
    if (it != Users.end()) {
        res.Name = it->second.GetName();
        res.Defeats = it->second.GetDefeats();
        res.Wins = it->second.GetWins();
        res.TotalGames = it->second.GetTotalGames();
    }
    return res;
}

std::pair<uint16_t, uint16_t> TState::GetRoomSize(const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    auto it = Rooms.find(roomName);
    if (it == Rooms.end()) {
        return {0, 0};
    }
    return it->second.GetRoomSize();
}

EUserStep TState::IsUserStep(const std::string& userName, const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    auto it = Rooms.find(roomName);
    if (it == Rooms.end()) {
        return EUserStep::RoomNotFound;
    }
    if (it->second.IsUserStep(userName)) {
        return EUserStep::UsersStep;
    } else {
        return EUserStep::NotThatUserStep;
    }
}

TUserState TState::GetUserState(const std::string& userName, const std::string& roomName) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    auto it = Rooms.find(roomName);
    if (it == Rooms.end()) {
        return {};
    }
    return it->second.GetUserState(userName);
}

TRoomsPair TState::GetRoomsLists(const std::string& user) {
    shared_lock globalLock(Globalmutex);
    shared_lock roomsLock(RoomsMutex);
    std::vector<std::string> waiting, gaming;
    for (auto& room : Rooms) {
        if (!room.second.GetIsStarted()) {
            waiting.push_back(room.first);
        } else if (room.second.HasMember(user)) {
            gaming.emplace_back(room.first);
        }
    }
    return std::make_pair(waiting, gaming);
}
