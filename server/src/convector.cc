#include <boost/endian/conversion.hpp>
#include <iostream>

#include "convector.h"

template<class Type>
std::vector<char> NumToData(Type x) {
    std::vector<char> res(sizeof(x));
    res.resize(sizeof(x));
    auto net = boost::endian::native_to_big(x);
    char net_char[sizeof(x)];
    std::memcpy(net_char, &net, sizeof(x));
    for (size_t i = 0; i < sizeof(x); ++i) {
        res[i] = net_char[i];
    }
    return res;
}

template<class Type>
Type DataToNum(char* buf) {
    Type x;
    memcpy(&x, buf, sizeof(x));
    return boost::endian::big_to_native(x);
}

std::uint8_t GetUi8(asio::ip::tcp::socket& sock) {
    using Type = std::uint8_t;
    char buf[sizeof(Type)];
    sock.receive(asio::buffer(buf, sizeof(Type)));
    return DataToNum<Type>(buf);
}

std::uint16_t GetUi16(asio::ip::tcp::socket& sock) {
    using Type = std::uint16_t;
    char buf[sizeof(Type)];
    sock.receive(asio::buffer(buf, sizeof(Type)));
    return DataToNum<Type>(buf);
}

std::uint32_t GetUi32(asio::ip::tcp::socket& sock) {
    using Type = std::uint32_t;
    char buf[sizeof(Type)];
    sock.receive(asio::buffer(buf, sizeof(Type)));
    return DataToNum<Type>(buf);
}

std::uint64_t GetUi64(asio::ip::tcp::socket& sock) {
    using Type = std::uint64_t;
    char buf[sizeof(Type)];
    sock.receive(asio::buffer(buf, sizeof(Type)));
    return DataToNum<Type>(buf);
}

std::string GetString(asio::ip::tcp::socket& sock) {
    auto size = GetUi16(sock);
    auto buf = new char[size];
    sock.receive(asio::buffer(buf, size));
    auto res = std::string(buf, size);
    delete[] buf;
    return res;
}

std::vector<uint8_t> GetRaw(asio::ip::tcp::socket& sock, size_t size) {
    std::vector<uint8_t> res(size);
    sock.receive(asio::buffer(res, size));
    return res;
}

std::vector<std::string> GetListString(asio::ip::tcp::socket& sock) {
    auto size = GetUi16(sock);
     std::vector<std::string> res;
     res.reserve(size);
     for (size_t i = 0; i < size; ++i) {
         res.emplace_back(GetString(sock));
     }
     return res;
}

void SendUi8(asio::ip::tcp::socket& sock, std::uint8_t x) {
    sock.send(asio::buffer(NumToData(x)));
}

void SendUi16(asio::ip::tcp::socket& sock, std::uint16_t x) {
    sock.send(asio::buffer(NumToData(x)));
}

void SendUi32(asio::ip::tcp::socket& sock, std::uint32_t x) {
    sock.send(asio::buffer(NumToData(x)));
}

void SendUi64(asio::ip::tcp::socket& sock, std::uint64_t x) {
    sock.send(asio::buffer(NumToData(x)));
}

std::vector<char> StringToData(const std::string& x) {
    auto res = NumToData<std::uint16_t>(x.size());
    res.resize(2 + x.size());
    for (size_t i = 0; i < x.size(); ++i) {
        res[2 + i] = x[i];
    }
    return res;
}


void SendString(asio::ip::tcp::socket& sock, const std::string& x) {
    sock.send(asio::buffer(StringToData(x)));
}

void SendRawString(asio::ip::tcp::socket& sock, const std::string& x) {
    std::vector<char> buf(x.begin(), x.end());
    sock.send(asio::buffer(buf));
}

void SendListString(asio::ip::tcp::socket& sock, const std::vector<std::string>& list) {
    SendUi16(sock, static_cast<uint16_t>(list.size()));
    for (const auto& str : list) {
        SendString(sock, str);
    }
}
