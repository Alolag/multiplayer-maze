#include <asio.hpp>
#include <cstdint>
#include <string>

void SendUi8(asio::ip::tcp::socket& sock, std::uint8_t x);
void SendUi16(asio::ip::tcp::socket& sock, std::uint16_t x);
void SendUi32(asio::ip::tcp::socket& sock, std::uint32_t x);
void SendUi64(asio::ip::tcp::socket& sock, std::uint64_t x);
void SendString(asio::ip::tcp::socket& sock, const std::string& x);
void SendListString(asio::ip::tcp::socket& sock, const std::vector<std::string>& list);
void SendRawString(asio::ip::tcp::socket& sock, const std::string& x);

std::uint8_t GetUi8(asio::ip::tcp::socket& sock);
std::uint16_t GetUi16(asio::ip::tcp::socket& sock);
std::uint32_t GetUi32(asio::ip::tcp::socket& sock);
std::uint64_t GetUi64(asio::ip::tcp::socket& sock);
std::string GetString(asio::ip::tcp::socket& sock);
std::vector<uint8_t> GetRaw(asio::ip::tcp::socket& sock, size_t size);
std::vector<std::string> GetListString(asio::ip::tcp::socket& sock);
