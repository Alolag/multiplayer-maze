#include <boost/bind.hpp>
#include <iostream>

#include "server.h"
#include "connection.h"

TServer::TServer(TState& state, unsigned short port)
    : State(state)
    , Service()
    , Acceptor(Service, asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port))
{
    std::cout << "Server listening port: " << port << std::endl;
}

TServer::~TServer() {
    Stop();
}

void TServer::Stop() {
    State.Save([&](){
        ThreadsGroup.interrupt_all();
        Service.stop();
    });
}

void handler_accept(TConnection::ptr client, const TConnection::error_code& err) {
    auto new_client = client->New();
    new_client->GetThreadsGrop().create_thread(boost::bind(&TConnection::Start, client));
    new_client->GetAcceptor().async_accept(new_client->GetSock(), boost::bind(handler_accept, new_client, _1));
}

void TServer::Start() {
    auto client = TConnection::New(State, Service, Acceptor, ThreadsGroup);
    Acceptor.async_accept(client->GetSock(), boost::bind(handler_accept, client, _1));
    ThreadsGroup.create_thread(boost::bind(&asio::io_service::run, &Service));
}
