#pragma once
#include <boost/thread/shared_mutex.hpp>
#include <map>

#include "config.pb.h"
#include "user.h"
#include "room.h"

class TConnection;
using TRoomsPair = std::pair<
    std::vector<std::string>,
    std::vector<std::string>
>;

enum class EGetInRoomResult {
    RoomNotFound,
    EnterInGamingRoom,
    UserNotInGamingList,
    GetInWaitingRoom,
};

enum class EUserStep {
    RoomNotFound,
    UsersStep,
    NotThatUserStep,
};

struct TSimpleUser {
    std::string Name;
    uint64_t Wins;
    uint64_t Defeats;
    uint64_t TotalGames;

    TSimpleUser(const TUser& user)
        : Name(user.GetName())
        , Wins(user.GetWins())
        , Defeats(user.GetDefeats())
        , TotalGames(user.GetTotalGames())
    {}

    TSimpleUser() = default;
};

enum class ERoomState {
    Canceled,
    Waiting,
    Gaming,
};

class TState {
    const std::string StateFileName;
    boost::shared_mutex UsersMutex;
    boost::shared_mutex ConnectionsMutex;
    boost::shared_mutex Globalmutex;
    boost::shared_mutex LeaderBoardMutex;
    boost::shared_mutex RoomsMutex;
    std::map<std::string, TUser> Users;
    std::set<TConnection*> Connections;
    std::vector<std::string> LeaderBoard;
    std::map<std::string, TRoom> Rooms;

    const uint32_t DefaultLives;
    const uint32_t DefaultAmmunitions;
    const uint32_t DefaultMonsterCounts;
    const uint8_t DefaultViewDistance;

private:
    void UpdateLeaderBoardWithoutMutex(const std::vector<std::string>& users);

public:
    const std::string SrverName;

    TState(const PConfig& config);

    bool AddUser(const std::string& name, const std::string& password);
    bool CheckUser(const std::string& name, const std::string& password);
    std::vector<TSimpleUser> GetLeaderBoard();
    TRoomsPair GetRoomsLists(const std::string& user);
    TSimpleUser GetUserInfo(const std::string& name);
    void AddConnections(TConnection*);
    bool CheckRoomSize(uint16_t width, uint16_t height) const;
    EGetInRoomResult GetInRoom(const std::string& userName, const std::string& roomName);
    EUserStep IsUserStep(const std::string& userName, const std::string& roomName);
    TUserState GetUserState(const std::string& userName, const std::string& roomName);
    ERoomState GetRoomState(const std::string& roomName);
    EMoveResult DoMove(const std::string& userName, const std::string& roomName, EMoveDirection dir);
    std::pair<uint16_t, uint16_t> GetRoomSize(const std::string& roomName);
    bool AddRoom(const std::string& creatorName, const std::string& name, uint16_t width, uint16_t height);
    bool DeleteRoom(const std::string& user, const std::string& roomName);
    bool DeleteMember(const std::string& user, const std::string& roomName);
    bool StartRoom(const std::string& user, const std::string& roomName);
    std::vector<std::string> GetMembersList(const std::string& roomName);
    void DeleteConnection(TConnection*);
    void Save(std::function<void()>);
    void UpdateLeaderBoard(const std::vector<std::string>& users);
    void ShowConnections();
};
