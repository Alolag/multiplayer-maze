#pragma once
#include <asio.hpp>
#include <boost/thread.hpp>

#include "state.h"

class TConnection : asio::noncopyable {
    std::string User;
    TState& State;
    asio::io_service& Service;
    asio::ip::tcp::socket Sock;
    asio::ip::tcp::acceptor& Acceptor;
    boost::thread_group& ThreadsGroup;
    std::string RoomName;
    bool IsConnected_ = false;

    enum class EAuth {
        Registration,
        Autorisation,
    };

private:
    void LogSign(EAuth);
    void SendLeaderBoard();
    void CreateRoom();
    void SendRoomsLists();
    void GetInRoom();
    void DoMove();

public:
    typedef std::shared_ptr<TConnection> ptr;
    typedef asio::error_code error_code;

    TConnection(TState&, asio::io_service&, asio::ip::tcp::acceptor&, boost::thread_group&);
    ~TConnection();
    static ptr New(TState& state, asio::io_service& service, asio::ip::tcp::acceptor&, boost::thread_group&);
    ptr New() const;
    asio::ip::tcp::socket& GetSock();
    asio::ip::tcp::acceptor& GetAcceptor();
    boost::thread_group& GetThreadsGrop();
    void Start();
    const std::string& GetName() const;
    bool IsConnected() const;

};

