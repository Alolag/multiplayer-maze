#include <experimental/filesystem>
#include <fstream>
#include <google/protobuf/util/json_util.h>
#include <iostream>

#include "config.pb.h"
#include "server.h"
#include "state.h"

namespace fs = std::experimental::filesystem;

constexpr auto CONFIG_FILE_NAME = "server_config.json";

int main() {
    PConfig config;
    if (fs::exists(CONFIG_FILE_NAME)) {
        std::ifstream config_file(CONFIG_FILE_NAME);
        std::stringstream buf;
        buf << config_file.rdbuf();
        google::protobuf::util::JsonStringToMessage(buf.str(), &config);
    } else {
        config.set_port(15123);
        config.set_statefile("server.state");
        config.set_servername("The best server");

        config.set_defaultlives(3);
        config.set_defaultammunitions(3);
        config.set_defaultviewdistance(3);
        config.set_defaultmonstercounts(3);

        google::protobuf::util::JsonPrintOptions options;
        options.add_whitespace = true;
        options.always_print_primitive_fields = true;
        options.preserve_proto_field_names = true;
        std::string json;
        google::protobuf::util::MessageToJsonString(config, &json, options);

        std::ofstream config_file;
        config_file.open(CONFIG_FILE_NAME);
        config_file << json;

        config_file.close();
        std::cout << "Server started first time. Check file `" <<
            CONFIG_FILE_NAME << "` and run server again" << std::endl;
        return 0;
    }
    TState state(config);
    TServer server(state, static_cast<unsigned short>(config.port()));
    server.Start();
    std::cout << "Started server: " << state.SrverName << std::endl;
    while (std::cin) {
        std::string str;
        std::cin >> str;
        if (str == "save") {
            state.Save([](){return;});
        } else if (str == "stop") {
            break;
        } else if (str == "users") {
            state.ShowConnections();
        } else {
            std::cout << "Unknown command" << std::endl;
        }
    }
}
