#include "user.h"

TUser::TUser(PUser const& in)
    : Name(in.name())
    , Password(in.password())
    , Wins(in.wins())
    , Defeats(in.defeats())
    , TotalGames(in.totalgames())
{}

TUser::TUser(const std::string& name, const std::string& password) 
    : Name(name)
    , Password(password)
{}

bool TUser::CheckPassowrd(const std::string& password) {
    return password == Password;
}

void TUser::Save(PUser* user) const {
    user->set_name(Name);
    user->set_wins(Wins);
    user->set_defeats(Defeats);
    user->set_totalgames(TotalGames);
    user->set_password(Password);
}

const std::string& TUser::GetName() const {
    return Name;
}

uint64_t& TUser::GetWins() {
    return Wins;
}

uint64_t& TUser::GetDefeats() {
    return Defeats;
}

uint64_t& TUser::GetTotalGames() {
    return TotalGames;
}

const uint64_t& TUser::GetWins() const {
    return Wins;
}

const uint64_t& TUser::GetDefeats() const {
    return Defeats;
}

const uint64_t& TUser::GetTotalGames() const {
    return TotalGames;
}
