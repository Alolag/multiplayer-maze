#pragma once
#include <string>
#include <boost/thread.hpp>
#include <libtcod/libtcod.hpp>

#include "room.pb.h"

enum class EMoveDirection {
    Up = 0,
    Right = 1,
    Left = 2,
    Down = 3,
};

enum class EMoveResult {
    Moved,
    UserNotFound,
    NotMoved,
    NotYouStep,
    Win,
    AllHumansKilled,
};

struct TMemberGameInfo {
    std::string Name;
    uint16_t X;
    uint16_t Y;
    uint32_t Lives;
    uint32_t Ammunitions;
    uint8_t ViewDistance;
    uint8_t AmmunitionsDistance;
    bool IsAlive;
    bool IsMonster;

    TMemberGameInfo() = default;
    TMemberGameInfo(const std::string& name);
    TMemberGameInfo(const PMemberGameInfo& user);
    void Save(PMemberGameInfo* user) const;
    bool operator==(const std::string& user) const;
};

struct TUserState {
    uint32_t Lives;
    uint32_t Ammunitions;
    std::string View;
    uint16_t WidthView;
    uint16_t HeightView;
    uint16_t X;
    uint16_t Y;
    bool UserNotFound = true;
    bool HasCoordinate;
};

class TRoom {
    const std::string Name;
    const std::string CreatorName;
    std::vector<TMemberGameInfo> Members;
    uint32_t Step;
    const uint16_t Width;
    const uint16_t Height;
    std::string Map;
    TCODMap FovMap;
    bool IsStarted;
    boost::shared_mutex Mutex;

private:
    std::pair<uint16_t, uint16_t> GetCoordinate(std::string::iterator);
    bool AiStep();
    void FixStep();
    void GenerateMap();
    void FillMembersCoordinate();
    void GenerateFovMap();

public:
    TRoom(const PRoom& room);
    TRoom(const std::string& name, const std::string& creatorName, uint16_t width, uint16_t height);
    void Save(PRoom* room);
    bool HasMember(const std::string& name);
    void AddMember(std::string user);
    bool GetIsStarted();
    bool DeleteMember(const std::string& name);
    bool IsUserStep(const std::string& name);
    EMoveResult DoMove(const std::string& name, EMoveDirection dir);
    std::vector<std::string> GetMembersName();
    std::vector<std::string> GetMembersNameWithLock();
    std::pair<uint16_t, uint16_t> GetRoomSize();
    const std::string& GetCreatorName();
    boost::shared_mutex& GetMutex();
    std::vector<TMemberGameInfo>& GetMembers();
    TUserState GetUserState(const std::string& user, bool usingCoordinate = true);
    void Start(std::function<void()> func);
    const std::string& GetName();
};
